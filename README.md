# redis

On Ubuntu 20.04 apt repos install Redis v5.0.7-2.  
On Ubuntu 18.04 apt repos install Redis v4.0.9-1ubuntu0.2.

Other than needing to add a `systemd restart` handler, I have not noticed
any needed configuration changes between Redis v4 and v5.


##  Memory caching (local cache)

https://docs.nextcloud.com/server/13/admin_manual/configuration_server/caching_configuration.html
```
taha@luxor:~
$ sudo apt install php-apcu
The following NEW packages will be installed:
  php-apcu
0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
```

Also install Redis (we will use APCu for local caching, and Redis for file locking).
```
taha@luxor:/
$ sudo apt install redis-server php-redis 
The following additional packages will be installed:
  libjemalloc1 php-igbinary redis-tools
The following NEW packages will be installed:
  libjemalloc1 php-igbinary php-redis redis-server redis-tools
0 upgraded, 5 newly installed, 0 to remove and 0 not upgraded.
Need to get 786 kB of archives.
After this operation, 3609 kB of additional disk space will be used.
Created symlink /etc/systemd/system/redis.service → /lib/systemd/system/redis-server.service.
Created symlink /etc/systemd/system/multi-user.target.wants/redis-server.service → /lib/systemd/system/redis-server.service.
```
Note: Redis runs on port `127.0.0.1:6379`, and you can configure Nextcloud to talk to it by specifiying `'redis' => array('host' => 'localhost', 'port' => 6379,),` in the config.
But since Redis is on the same host as Nextcloud, using a UNIX socket would be faster.
https://bayton.org/docs/nextcloud/installing-nextcloud-on-ubuntu-16-04-lts-with-redis-apcu-ssl-apache/

Edit `/etc/redis/redis.conf`, find `port 6379` and change it to `port 0`, then uncomment
```
unixsocket /var/run/redis/redis-server.sock
unixsocketperm 700
```
changing permissions to `770` at the same time. 
Add the Apache user to the redis group: `sudo usermod -a -G redis www-data`

Now restart Apache and added the following lines to Nextcloud config file:
```
'memcache.local' => '\OC\Memcache\Redis',
'memcache.locking' => '\OC\Memcache\Redis',
'redis' => array(
     'host' => '/var/run/redis/redis-server.sock',
     'port' => 0,
     'timeout' => 1.5,
      ),
```


## Warnings in the redis log

I noticed today after upgrading PHP from 7.3 to 7.4 and subsequent reboot.


`/var/log/redis/redis-server.log`:
```
1539:M 04 Apr 17:49:57.157 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
1539:M 04 Apr 17:49:57.157 # Server initialized
1539:M 04 Apr 17:49:57.157 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or ru
n the command 'sysctl vm.overcommit_memory=1' for this to take effect.
1539:M 04 Apr 17:49:57.157 # WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo nev
er > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after a reboot. Redis must be restarted after THP is disabled.
```

We may want to follow the recommendations above.


## Refs

+ https://github.com/DavidWittman/ansible-redis/
+ https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/caching_configuration.html
+ https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-20-04
+ https://www.caretech.io/2021/05/20/how-to-install-nextcloud-21-on-ubuntu-server-20-04-with-nginx-php-fpm-mariadb-and-redis/
+ https://bayton.org/docs/nextcloud/installing-nextcloud-on-ubuntu-16-04-lts-with-redis-apcu-ssl-apache/
+ https://kb.linuxlove.xyz/peertube.html
+ https://wiki.archlinux.org/title/Redis
